# Group and project presentation

## About our team

Our team is composed of :

- [Xu Ze-Xuan](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/zexuan.xu):   Xu Ze-Xuan is very interested in programming and microcontrollers, which is why he chose this lesson and the microcontroller part. He is in charge of the Arduino programming.
- [Gaillet Gauthier](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/gauthier.gaillet): Gauthier is in his final year of a civil engineering course with a focus on construction. 
- [Mayorga Montaguano Jacob](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/jacob.mayorga): Jacob is studying Biology, and he is particularly fascinated by the 3D printer, among other lessons. He is responsible for capturing images to illustrate the ongoing process.

Here we are :

![](images/photo_grp.jpg)

## Our Project

### Abstract

PFAS, or per- and polyfluoroalkyl substances, represent a pressing concern in our food landscape. Widely used in everyday products, these chemicals find their way into our meals through food packaging, cookware coatings, and even agricultural practices. Notorious for their water and grease-resistant properties, PFAS contaminate the food chain, posing potential health risks to consumers. Cooking utensils with non-stick coatings can release PFAS during food preparation, contributing to human exposure. Studies have linked PFAS exposure to various chronic health issues, making it crucial to address their presence in our food supply. Growing regulatory efforts seek to limit the use of PFAS in food-related contexts, emphasizing the importance of informed choices and sustainable practices in our kitchens

Our project is based on the problematic question of how to prevent PFAS in our foods. To do so, we have focused on the tools used for cooking. The easiest way to prevent PFAS in our food is to avoid them in our kitchen ustensils.

Our project is about the third Sustainable Developement Goal (SDG) of the United Nations namely *[Good Health and Well-being](https://sdgs.un.org/goals/goal3)*.

We will present an alternative to the PFAS anti-adhesive frying pan and compare their efficiency and whether they are really anti-adhesive.

![Graphical abstract](images/logo.png)
