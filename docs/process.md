## 1. What are PFAS

PFAS stands for Per- and Polyfluoroalkyl Substances. They are a group of synthetic organic compounds that contain fluorine-carbon bonds. These substances are chemicaly very stable, thermal resistant, and water and oil repellent. PFAS can be classified into two main types: perfluoroalkyl substances (PFASs) and polyfluoroalkyl substances (PFOSs). The chemical composition is depicted in the following figure.

![](images/pfas_molecule.png)

### 1.1 Where are they found

PFAS have been widely used in various industrial and consumer products due to their advantageous properties as mentionned in the previous paragraph. Common applications include:

- Water- and Oil-Repellent Products: PFAS are used in the production of water- and oil-resistant coatings for textiles, carpets, paper and pans. Pans that present PFAS will be the main subject of this work.

- Firefighting Foam: Certain PFAS, such as perfluorooctanoic acid (PFOA) and perfluorooctanesulfonic acid (PFOS), have been used in firefighting foams, particularly in training exercises at military bases and airports.

- Food Packaging: PFAS have been used in the production of food packaging materials, such as non-stick coatings on cookware and food wrappers.

- Electronics and Semiconductor Manufacturing: PFAS have been used in the manufacturing of electronics and semiconductors.

Other application of PFAS can be found on the following picture [[1]](https://www.inspq.qc.ca/pfas/pfas-definition-et-utilisation).

![](images/pfas_ou.jpg)

### 1.2 Human Health Risks

PFAS offer advantages, but there are rising worries about how they may affect the environment and public health. Some risks include:

- Bioaccumulation: PFAS have a tendency to accumulate in living organisms, including humans, and can persist in the environment for long periods.

- Potential Health Effects: Exposure to certain PFAS has been associated with adverse health effects, including developmental issues, effects on the immune system, liver damage, and an increased risk of certain cancers.

- Contamination of Drinking Water: PFAS can contaminate water supplies, especially in areas near industrial sites, military bases, and firefighting training areas. This contamination can lead to exposure through drinking water and the consumption of contaminated fish and crops.

- Global Distribution: PFAS are found globally, and their widespread use has resulted in their presence in air, water, soil, and wildlife across various regions.

- Regulatory Response: Due to the concerns surrounding PFAS, regulatory agencies in several countries (e.g. Belgium) have taken measures to restrict the use of certain PFAS and to set guidelines for acceptable levels in drinking water.

In conclusion, PFAS are a class of man-made substances whose distinct qualities have made them widely used in a variety of consumer and industrial applications. But because of their tenacity, bioaccumulation, and possible health hazards, there are now more regulatory measures and environmental and public health issues, which have prompted more investigation.

Sources:

- Institut National de Santé du Québec [[1]](https://www.inspq.qc.ca/pfas/effets-sur-la-sante-fiche-technique)
- European Environment Agency [[2]](https://www.eea.europa.eu/fr/help/questions-frequemment-posees/que-sont-les-pfas-et)
- Agence Régionale de Santé Auvergne-Rhône-Alpes [[3]](https://www.auvergne-rhone-alpes.ars.sante.fr/mieux-comprendre-les-pfas-leurs-effets-les-textes-applicables#:~:text=Les%20effets%20des%20PFAS%20sur%20la%20sant%C3%A9&text=effet%20sur%20le%20syst%C3%A8me%20immunitaire,perturbation%20du%20fonctionnement%20du%20foie.)

## 2. How to prevent PFAS in our food

Since our project concerns pans containing PFAS, an explanation must be given on how to prevent PFAS from getting into our food.

People can take a number of actions to reduce their exposure PFAS in food. Select cookware (such as stainless steel or ceramic choices) free of PFAS-containing nonstick coatings, and steer clear of processed meals and fast food containers that may have come into contact with PFAS. To lower PFAS levels, choose natural fiber food storage options like glass or stainless steel, read labels for PFAS-free indications, and filter drinking water. Other tactics include washing fruits and vegetables, choosing personal care items without PFAS, and pushing for laws that would limit the use of PFAS in consumer goods. Keeping up with PFAS-free companies and endorsing them promotes reform throughout the industry. Staying abreast of emerging research and guidelines while consulting with healthcare professionals and environmental organizations can provide ongoing insights into minimizing PFAS exposure.

## 3. Group project

The aim of this group work is firstly to study the anti-adhesiveness of frying pans through three small experiments and secondly to convince people that there are alternatives to frying pans containing PFAS which are much more beneficial to health without losing the effectiveness of frying pans containing PFAS (e.g. Téfal pans). The studied parameters are the heat transfer (the heating and cooling rate), the coefficient of static friction and the surface tension. Three pans will be studied. A Tefal pan (containing PFAS), a ceramic pan and a steel pan that has been **seasoned**. Seasoning a pan is a process that involves coating the pan with a thin layer of oil and heating it to create a protective layer on the surface. This layer helps prevent food from sticking and improves the pan's non-stick properties. Here are some *theoretical* steps to season a pan:

- Coat the entire surface of the pan, including the exterior and handle, with a thin layer of cooking oil. Common choices for seasoning include vegetable oil (except olive oil), canola oil, or flaxseed oil.
- Place the pan in an oven preheated to a high temperature.
- Turn off the oven and let the pan cool inside. Once it's cool enough to handle, check the surface. It should appear darker and have a smoother texture.

The following picture shows a pan before and after the seasoning process [[4]](https://fr.wikipedia.org/wiki/Culottage).

![](images/culottage.png)

### 3.1 Seasoned pan

We created our own seasoned pan. Instead of seasoning it in an oven, we opted to use fire, as explained on this [site](https://www.cerfdellier.com/content/305-culottage-poeles-en-acier). 

Here are the steps to follow :

* Firstly, wash the pan with hot water and then dry it. 

* Secondly, apply sunflower oil (olive oil is not recommended) and cook it over low heat until you see a bit of vapor appearing (ensure to open windows or activate ventilation); this process takes 8-10 minutes. Allow it to cool for a moment, then transfer the excess oil to a container.

* Once cooled, wipe the pan with absorbent paper towels to remove any remaining oil. 

* Finally, cook it for an additional 2 minutes over low heat. You can repeat this process twice for greater effectiveness and durability (we only did it once). 

Here is the before and after result: 

This seasoned pan was used in the experiment described in 3.3.1.

![Alt text](images/PAN1.png)

As comparison the following image shows a seasoned pas from Jason's restaurant. This pan was used during the experiment explained in section 3.3.2.

![Alt text](images/p.jpg)

### 3.2 Heat transfer

#### 3.2.1 Introduction

Heat transfer is crucial in a pan for several reasons, playing a fundamental role in the cooking process. Understanding and managing heat transfer is essential for achieving the desired results in food preparation. Here are key reasons why heat transfer is important in a pan:

- Cooking Efficiency: Heat transfer allows the pan to efficiently transfer thermal energy from the heat source (burners or heating elements) to the cooking vessel. Efficient heat transfer ensures that the cookware gets hot, allowing for effective cooking.

- Uniform Cooking: Proper heat transfer ensures even distribution of heat across the cooking surface. This is essential to prevent uneven cooking, undercooked or overcooked areas in the food, and to achieve consistent taste and texture.

- Control over Temperature: Heat transfer enables cooks to control and adjust the temperature of the cooking surface. Different cooking techniques and recipes often require specific temperature levels, and the ability to modulate heat transfer allows for precise temperature control.

- Safety: Understanding heat transfer is crucial for safe cooking. Proper heat transfer prevents hotspots that could cause burns or uneven cooking. It allows for a more predictable cooking environment, reducing the risk of accidents.

- Heat Preservation: Once heat is transferred to the cookware, it plays a role in preserving the temperature, keeping food warm even after cooking. This is particularly important for serving hot meals.

To do this, we're going to study heat transfer in different pans using a fabricated infrared thermometer we've designed. 

#### 3.2.2 Infrared thermometer

  To determine the conductivity of each pan, we created an infrared thermometer to observe the evolution of temperature in each pan during a certain amount of time on the heat source. The heat source was an induction hob set to maximum temperature. Afterwards the evolution of the temperature was also measured during a cooling phase were the pan was removed from the heating source.

##### 3.2.2.1 Component

  The infrared (IR) thermometer is composed of :

* A microcontroller to manage and provide energy to others components.
  
  This is the main componant of the arduino system. It has a pin to output energy and an USB port to accept energy. It can accept code written in arduino to execute it when it is provided with energy.

![](images/nano.jpg)

* A digital LED to show the temperature the sensor module has captured.
  
  This is a 7 segmented LED which some pins control a segment of a number and, some pins control the number itself. This type of LED is challenging to control because energy needs to be send to a pin to select the number to control and some pins to select segments to light up. Moreover the number selected need to be switched quickly  to be able to make a number bigger than 9.

![](images/led.jpg)

* An IR temperature sensor module to capte temperature of items.
  
  This sensor uses infrared to mesure the temperature of items .
  A module  needs to be used to control the sensor.

  ![](images/sensor.jpg)

* Some jumpers to connect different components together.

##### 3.2.2.2 Assembled thermometer

  This is what the assembled thermometer looks like. It is very hard to see the connections between components on a simple image as there are too many jumpers.

![](images/assembled.jpg)

![](images/plan.png)

In the above depicted image, the sensor exhibits a connection configuration with the Arduino, where both VIn and ground (gnd) are linked. Additionally, there are two analog pins that necessitate a connection to the Arduino. However, it is crucial to note that these pins also serve as the serial data (SDA) and serial clock (SCL) lines. Consequently, it becomes imperative to establish connections for these analog pins not only to the analog pins on the Arduino but also to the SDA and SCL analog pins on the Arduino, considering their dual functionality in both analog data transmission and clock synchronization. This careful attention to the dual roles of these pins ensures proper communication and functionality within the system.

Regarding the LED, it's essential to understand that each pin of the LED corresponds to illuminating a specific segment of the LED display.

##### 3.2.2.3 Box

To make our thermometer more aesthetically pleasing, we've designed a box enclosing all the thermometer's components. This box has been designed in the OpenScad software.

  ![](images/box_plan.png) 

We have designed the hole for chargin the arduino, hole for the LED and hole for the sensor.

We also have holes on the side of the box to stuck the box and his cover.

This is the first draft our printed model. The box presents mutliple default as a width too short, not tight enough joints or not big enough holes.

![](images/box.jpg)

After correcting these defaults we arrived at this final prototype.

![](images/image1.jpg)

  We have enlarged the box and adjusted the holes for sensors and USB port.



#### 3.2.3 Experiment

The aim is to stabilise the temperature in the pan as quickly as possible when our induction hob is at its maximum (number 9 in this case). To test this, we will study the temperature evolution over a period of one minute. Our induction hob was turned up to maximum and we waited a few minutes for the temperature emitted by the induction hob to stabilise. The various pans were cooled down and their temperature was measured over a period of one minute.

#### 3.2.4 Results

The following plot summerizes the heating and cooling phases of the three different pans. The error of the sensor on each measure is also represented on the plot through the [data sheet of the sensor](https://www.tinytronics.nl/shop/en/sensors/temperature/non-contact-ir-temperature-sensor-module-mlx90614-baa-90).

![](images/graph.png)

We can observe that the thermal conductivity of the seasoned pan is the highest, meaning that cooking aliments will theoretically be faster.

The errors on each measurement can be explained by

- Thermal effects on internal electronic components (e.g. thermal expansion)
- External thermal effects (e.g. convection, radiation)
- Inappropriate  calibration of the IR thermometer.

#### 3.2.5 Cooking test

In order to visualize the thermal conductivity of each pan an to quantify cooking efficiency of each, we did a cooking test using chicken.

The experiment is very simple. An equal amount of butter is melted in each pan. Once the butter  has melted in the pan, a small cube of chicken about 3 cm high is cooked in the pan for 1 minute and 30 seconds on both sides.

At the end of the cooking time, a ruler is used to measure the height of the cooked part of the chicken.  The following table summerizes the results.

| Type of pan     | Height [mm] |
|:--------------- |:-----------:|
| Ceramic pan     | 4           |
| Pan with teflon | 5           |
| Seasoned pan    | 6           |

These results confirm the measurements plotted in the previous subsection.
The following pictures depicted the experiment.

![](images/poulet.png)

#### 3.2.6 Conclusion

As a conclusion and with the support of our measurements made with the thermometer of section 3.2.2 we can affirm that the cooking of food will be faster with a seasoned pan than with a pan containing Teflon. As well as having similar non-stick properties to Teflon frying pans, a coated frying pan has better thermal conductivity and is healthier.

### 3.3 Anti-adhesion testing

This chapter is dedicated to two experiences conducted to test the anti-adhesive properties of three different pans. Before starting the two experiments, we had to prepare the samples of the pans because it was preferable to carry out the following experiments with rectangular parts of the pans rather than the whole pans. After several hours and having tried a number of useful techniques, we managed to cut out rectangles. The rectangles will be used to determine the contact angle of a drop of water with the pan and the pan's coefficient of friction.

The figure below shows the cut-off pans. To illustrate the difficulty of this cut, 4 different tools were used (a jigsaw, a drill and two types of disceaser). During the manipulation, 4 jigsaw blades broke and two disceaser discs.

![](images/carnage1.jpg)
![](images/carnage2.jpg)

The two conducted experiences will now be described.

#### 3.3.1 Surface tension measurement

Surface tension is a property of the surface of a liquid that allows it to resist an external force. It arises from the cohesive forces between molecules in the liquid. The molecules at the surface of a liquid experience a net inward force due to the unbalanced cohesive forces with the molecules below the surface. This creates a "skin" or "film" on the surface, which gives rise to the phenomenon known as surface tension.

Surface tension is typically measured in force per unit length (e.g., $N/m$). The SI unit for surface tension is Newton per meter ($N/m$).

The angle of contact is a related concept, particularly when considering the behavior of a liquid interface in contact with a solid surface. The angle of contact is the angle formed between the liquid surface and the solid surface at the point of contact. It is an important parameter in understanding wetting phenomena.

This test will be carried out to compare the non-stick properties of different pans. To do this we selected 4 different pans; a pan with teflon, a ceramic pan, a steel pan and a seasoned pan.  The test consists of placing a small drop of water on a surface and measuring the angle formed between the drop and the surface of the pan as illustrated below [[5]](https://www.linseis.com/fr/grandeurs-mesurees/angle-de-contact/#:~:text=Angle%20de%20contact%20%E2%80%93%20Mesure%20de,la%20nature%20de%20la%20surface.). The greater the angle, the more non-stick the pan. The following picture illustrates this concept [[6]](https://www.linseis.com/fr/grandeurs-mesurees/angle-de-contact/).

![](images/angle_contact.png)

##### 3.3.1.1 Teflal pan

After placing a droplet on our Teflal pan, we used the ImageJ software to analyse the angle of contact. [ImageJ](https://imagej.net/ij/index.html) is open-source image analysis software that is widely used in the scientific field, particularly in biology and bioinformatics.

![](images/tefal.png)

At the end we arrived at a contact angle of 63,7°. The smaller the angle the better the pan has anti-adhesive properties. We can classified the contact angles in three categories:

- Acute Angle (θ < 90 degrees): In this case, the liquid wets the solid surface well. Examples include water on glass.

- Right Angle (θ = 90 degrees): The liquid and solid do not have a strong affinity for each other. Mercury on glass is an example.

- Obtuse Angle (θ > 90 degrees): The liquid tends to bead up on the solid surface. An example is water on a waxed car surface.

In this case we can conclude that the Tefal pan has good anti-adhesive properties as expected.

##### 3.3.1.2 Metalic pan

The second pan tested was the metal pan, in order to have a reference angle that would be compared with the angle obtained with the seasoned pan to check whether the metal pan had become non-stick. If the angle of the seasoned pan is greater than the angle of the metal pan, we can conclude that the process of seasoning a pan is well executed.

Remark: The yellow angle on the picture is an additional measurement. This is not the angle used for the comparaison between the metalic and seasoned pan.

![](images/metal.png)

The angle is equal to 69,4°.

##### 3.3.1.3 Seasoned pan

![](images/culotte.png)

We arrived at an angle of 63,1°. 

##### 3.3.1.3 Ceramic pan

Unfortunately we don't have any angle measurements for the ceramic peel because the ceramic pan is too large for the micrscope's experimental setup. We didn't want to cut out pans because, as well as being a difficult task, it takes up a lot of time and material.

#### 3.3.2 Discussion

First, after studying the surface tension of the different frying pans, we can conclude that there is no significant difference between the Tefal frying pan and the cast iron frying pan. We can therefore conclude that the non-stick properties of these two pans are similar. In addition to having the same non-stick properties as the Tefal pan from the point of view of this test, the seasoned iron pan has the advantage of not containing PFAS and is therefore healthier.

Second, the angle obtained with the metalic pan is greater (69,4°) than the angle obtained with the seasoned pan (63,1°). This means that the metalic pan is more antiadhesive than he seasoned pas, which not logical. 

There are two possible reasons for this. Firstly, the microscope didn't allow us to visualise whole pans (the pans were too large for the setup) and we didn't want to cut out pans because, as well as being a difficult task, it takes up a lot of time and material. Iron pans are also expensive. We therefore decided to use a piece of metal to replace the stove. The metal isn't exactly the same, but we thought we'd have a rough idea of the cost of metal stoves. 

The second reason may be that seasoning process was not done properly. The pan may not have been completely clean before the pan was seasoned.

#### 3.3.2 The coefficient of static friction

##### 3.3.2.1 Theoretical introduction

The coefficient of static friction is a dimensionless value that expresses the resistance to movement of an object at rest relative to a surface. More precisely, it measures the maximum static frictional force that must be overcome before an object at rest begins to move. The shorter the value of coefficient of static friction the better the anti-adhesive properties of the pan.

Scientifically, the coefficient of static friction $\mu$ is defined by the following formula :

$ \mu = \frac{F_2}{F}$

Where,

- $F$ is the static friction force in $[N]$
- $\mu$ is the coefficient of static friction which is dimensionless
- $F_2$ is the normal force in $[N]$

This formula is illustrated on the following sketch [[7]](https://www.ld-didactic.de/literatur/hb/f/p1/p1252_f.pdf).

![](images/sketch.png)

If the force applied to try to move the object is less than $F$
the object remains at rest. When the force applied reaches or exceeds this threshold, the object begins to move, and the dynamic friction coefficient becomes applicable to describe friction during motion. The dynamic friction coefficent in not relevant in the context of anti-adhesive properties of pans so it will not be studied.

The coefficient of static friction depends on the surface properties of the materials in contact. A rough or sticky surface will generally have a higher coefficient of static friction, requiring a greater force to overcome the initial static friction.

##### 3.3.2.2 Experimental setup

The experimental setup is very simple. The inclined plane is screwed onto a hinge, allowing it to rotate. A round square is fixed next to the hinge to prescisely read the angle at which the reference object begins to slide on the inclined plane. 

From this angle we can determine the coefficient of static friction $\mu$ by taking the tangent of this angle:

 $ \mu=tan(\alpha)$ 

 With $\alpha$ equals the angle in degrees at with the object begins to slide ($\alpha=tan(\frac{h}{s})$).

The inclined planes are compose of three different materials: ceramic, a part of a Tefal pan and a seasoned steel. The reference object is a little plastic rectanglular parallelipiped. For each pan different measures were taken. The final value for the coefficient of static friction is the mean value of the sample $\overline\mu$. 

A 3D printer was used in order to incorporate a learned Fablab tool in this short experience. The 3D printer was used to print the hinge. The hinge was not designed by one of the team members but was found online [[8]](https://www.thingiverse.com/thing:3563212). This hinge is licensed by Creative [Commons-Attributions](https://creativecommons.org/licenses/by/4.0/).

![](images/charniere.png)

After printing and assembling the expermiental setup is illustrated on the following picture.

![](images/charnierePrint.png)
![](images/montage.jpg)

##### 3.3.2.3 Results

| Material     | Mean static friction coefficient  $\overline\mu$ | Reference value from litterature                                                                                           | Mean angle $\overline\alpha$[°] |
|:------------ |:------------------------------------------------:|:--------------------------------------------------------------------------------------------------------------------------:|:------------------------------- |
| Steel        | 0,57                                             | 0,2-0,8 [[9]](https://dds.univ-lyon1.fr/icap_website/3649/72052)                                                           | 30                              |
| Tefal pan    | 0,25                                             | 0,05-0,09 [[10]](https://static.eriksgroup.com/fr/plastics/datasheets/fluores/ptfe/eriks%20-%20proprietes%20du%20ptfe.pdf) | 15                              |
| Seasoned pan | 0,15                                             | /                                                                                                                          | 13                              |
| Ceramic pan  | 0,43                                             | 0,3-0,4 [[11]](https://www.steeltechservices.be/file/si646614/Tribologie%20des%20c%C3%A9ramiques-fi33900330.pdf)           | 23                              |

##### 3.3.2.4 Discussion

These results must be taken with a critical mind because, as mentioned earlier, we have had counter-intuitive results with the surface tension measurement. In the case of a badly culled stove, the coefficient obtained is overestimated and does not reflect reality. However, as several measurements were made on different stoves, the figures reflect the coefficients of the pan used. 

### 3.4 Conclusions

This project has been beneficial for several reasons. Firstly, it was interesting to work on a topical issue that concerns most households. Secondly, it enabled us to gain a better understanding of PFAS in the culinary environment and it made the members of the group, as well as people outside the project, more aware of the effects of PFAS and their consequences on human health. We hope that this will lead to a change in some of our cooking habits, because as this project has shown, a non-stick pan has the same non-stick properties as a Teflon pan, but is better for human health. Thirdly, this project was an opportunity to put into practice some of the concepts we had learnt in class, such as the use of 3D drawing and 3D printing. Lastly, despite the fact that our group dynamic was a bit chaotic, we're taking it as a constructive experience and it will serve as a lesson for future group work.

Due to our lack of rigour in testing the surface area and the coefficient of friction using two different stoves with different bases, our results are slightly biased. This can be explained by the fact that nobody was really prepared to break a stove for surface tension. So we had to find an alternative.

However, based on the results obtained in this project, we can conclude that a correctly culled stove is just as efficient as a Teflon stove, as well as being better for your health. 

### 3.6 Outlook

This project can be improved by applying the following points in particular:

The first possible improvement would be to increase the sample of stoves used: In this project only 3 stoves were tested but other stoves made of other materials could also be tested in order to offer the public a greater number of alternatives to stoves containing teflon. 

The second point that could be improved is the number of experiments carried out and the number of parameters studied. In this project, only 3 parameters were studied (coefficient of static friction, surface tension and temperature). Other parameters could be studied. 

## 4. Group dynamics

Our group dynamics should not be used as an example to other groups. Indeed many negative points can summarize our group. First of all there is the lack of proactivity, dynamism and optimism. We were the last group to find a problematic topic and a feeling of defeat settled in and was omnipresent throughout the project. As a result, there was an unequal level of personal investment among the members of the group, resulting in a distribution of work that was not entirely equitable. A lack of creativity, constructive criticism and ideas was also present in the group. The fact of coming from other faculties can be beneficial but in our case it was not too much the case.

This group dynamism will certainly serve as a lesson for future group work. 

## 5. Other Sources

- Consolato Schiavone, Chiara Portesi 1, *PFAS: A Review of the State of the Art, from Legislation to Analytical Approaches and Toxicological Aspects for Assessing Contamination in Food and Environment and Related Risks*, 2023 [Link](https://www.mdpi.com/2076-3417/13/11/6696)
- Andreas Androulakakis, Nikiforos Alygizakis ORCID logoab, Erasmia Bizani a and Nikolaos S. Thomaidis, *Current progress in the environmental analysis of poly- and perfluoroalkyl substances (PFAS)*, 2022 [Link](https://pubs.rsc.org/en/content/articlehtml/2022/va/d2va00147k)
- United States Environmental Protection Agency, *PFAS explained* [Link](https://www.epa.gov/pfas/pfas-explained)
- Eva Solo, *What is the issue with PFAS and frying pans [Link](https://www.evasolo.com/gb/blog/what-is-the-issue-with-pfass-and-frying-pans) 
